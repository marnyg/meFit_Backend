﻿using AutoMapper;
using meFIT.BackendAPI.DTOs.Goal;
using meFIT.BackendAPI.DTOs.GoalWorkout;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Profiles
{
    public class GoalProfile: AutoMapper.Profile
    {
        public GoalProfile()
        {
            CreateMap<GoalDTO, Goal>().ReverseMap();
        }
    }
}
