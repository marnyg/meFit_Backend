﻿using AutoMapper;
using meFIT.BackendAPI.DTOs.Profile;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Profiles
{
    public class ProfileProfile : AutoMapper.Profile
    {
        public ProfileProfile()
        {
            CreateMap<ProfileDTO, Models.Profile>().ReverseMap();
        }
    }
}
