﻿using AutoMapper;
using meFIT.BackendAPI.DTOs.Workout;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Profiles
{
    public class WorkoutProfile : AutoMapper.Profile
    {
        public WorkoutProfile()
        {
            CreateMap<WorkoutDTO, Workout>().ReverseMap();
        }
    }
}
