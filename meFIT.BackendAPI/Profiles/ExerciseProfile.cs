﻿using AutoMapper;
using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Profiles
{
    public class ExerciseProfile : AutoMapper.Profile
    {
        public ExerciseProfile()
        {
            CreateMap<ExerciseDTO, Exercise>().ReverseMap();
        }
    }
}
