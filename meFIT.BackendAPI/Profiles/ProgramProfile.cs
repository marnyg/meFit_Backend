﻿using meFIT.BackendAPI.DTOs.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Profiles
{
    public class ProgramProfile : AutoMapper.Profile
    {
        public ProgramProfile()
        {
            CreateMap<ProgramDTO, Program>().ReverseMap();
        }
    }
}
