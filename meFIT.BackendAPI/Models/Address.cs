﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Address</c> models an address.
    /// </summary>
    public class Address : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string AddressLine1 { get; set; }

        [StringLength(100)]
        public string AddressLine2 { get; set; }
        [StringLength(100)]
        public string AddressLine3 { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 1)]
        public string PostalCode { get; set; }
        [Required]
        [StringLength(90, MinimumLength = 1)]
        public string City { get; set; }
        [Required]
        [StringLength(60, MinimumLength = 1)]
        public string Country { get; set; }
    }
}
