﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Workout</c> models a workout.
    /// </summary>
    public class Workout : IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public ICollection<Set> Sets { get; set; }
        public ICollection<WorkoutCategory> WorkoutCategories { get; set; }
    }
}
