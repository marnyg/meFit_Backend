﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>WorkoutCategory</c> models a joining table between <c>Workout</c> and <c>Category</c>.
    /// </summary>
    public class WorkoutCategory
    {
        public int WorkoutId { get; set; }
        public int CategoryId { get; set; }

        public Workout Workout { get; set; }
        public Category Category { get; set; }
    }
}
