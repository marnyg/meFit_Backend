﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Profile</c> models information about user including address and health information.
    /// </summary>
    public class Profile : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        // A user does not need to have an address registred
        public int? AddressId { get; set; }
        public Address Address { get; set; }

        /// <summary>
        /// Property <c>Weight</c> is in kilogram unit
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// Property <c>Height</c> is in centimeter unit
        /// </summary>
        public float Height { get; set; }

        [StringLength(255)]
        public string MedicalConditions { get; set; }
        [StringLength(255)]
        public string Disabilities { get; set; }

        public ICollection<Goal> Goals { get; set; }
    }
}
