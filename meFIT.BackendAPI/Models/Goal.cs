﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Goal</c> models a set of workouts. 
    /// </summary>
    public class Goal : IEntity
    {
        [Key]
        public int Id { get; set; }

        // A goal can be attaced to a program
        public int? ProgramId { get; set; }
        public Program Program { get; set; }
        public ICollection<GoalWorkout> GoalWorkouts { get; set; }

        public int ProfileId { get; set; }
        public Profile Profile { get; set; }    
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsCompleted { get; set; }
    }
}
