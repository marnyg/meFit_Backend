﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>ProgramCategory</c> models a joining table between <c>Program</c> and <c>Category</c>.
    /// </summary>
    public class ProgramCategory
    {
        public int ProgramId { get; set; }
        public int CategoryId { get; set; }

        public Program Program { get; set; }
        public Category Category { get; set; }
    }
}
