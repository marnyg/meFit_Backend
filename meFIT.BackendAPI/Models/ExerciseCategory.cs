﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>ExerciseCategory</c> models a joining table between <c>Exercise</c> and <c>Category</c>.
    /// </summary>
    public class ExerciseCategory
    {
        public int CategoryId { get; set; }
        public int ExerciseId { get; set; }

        public Category Category { get; set; }
        public Exercise Exercise { get; set; }
    }
}
