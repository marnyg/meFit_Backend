﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    //TODOS: Add email-address?

    /// <summary>
    /// Class <c>User</c> models the identity of a user and its roles.
    /// </summary>
    public class User : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Password { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        //Contributors does not need to be admin. Admin should have their own rights
        public bool IsContributer { get; set; }
        public bool IsAdmin { get; set; }
    }
}
