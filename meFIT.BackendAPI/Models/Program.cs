﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Program</c> models a workout program.
    /// </summary>
    public class Program : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        public ICollection<ProgramWorkout> ProgramWorkouts { get; set; }
        public ICollection<ProgramCategory> ProgramCategories { get; set; }
    }
}
