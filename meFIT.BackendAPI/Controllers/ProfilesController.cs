﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using AutoMapper;
using meFIT.BackendAPI.DTOs.Profile;

namespace meFIT.BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public ProfilesController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Profiles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProfileDTO>>> GetProfile()
        {
            var profilesList = await _context.Profile.ToListAsync();
            var profileDTOs = _mapper.Map<List<ProfileDTO>>(profilesList);
            return Ok(profileDTOs);
        }

        // GET: api/Profiles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProfileDTO>> GetProfile(int id)
        {
            var profile = await _context.Profile.FindAsync(id);

            if (profile == null)
            {
                return NotFound();
            }

            var profileDTO = _mapper.Map<ProfileDTO>(profile);
            return profileDTO;
        }

        // PUT: api/Profiles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProfile(int id, ProfileDTO profileDTO)
        {
            Models.Profile profileModel = _mapper.Map<Models.Profile>(profileDTO);
            if (id != profileModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(profileModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Profiles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ProfileDTO>> PostProfile(ProfileDTO profileDTO)
        {
            Models.Profile profileModel = _mapper.Map<Models.Profile>(profileDTO);
            _context.Profile.Add(profileModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProfile", new { id = profileModel.Id }, profileDTO);
        }

        // DELETE: api/Profiles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProfileDTO>> DeleteProfile(int id)
        {
            var profile = await _context.Profile.FindAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            _context.Profile.Remove(profile);
            await _context.SaveChangesAsync();
            var profileDTO = _mapper.Map<ProfileDTO>(profile);
            return profileDTO;
        }

        private bool ProfileExists(int id)
        {
            return _context.Profile.Any(e => e.Id == id);
        }
    }
}
