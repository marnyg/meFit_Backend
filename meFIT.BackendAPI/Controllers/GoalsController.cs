﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using AutoMapper;
using meFIT.BackendAPI.DTOs.Goal;

namespace meFIT.BackendAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GoalsController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public GoalsController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Goals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GoalDTO>>> GetGoal()
        {
            var goalsList = await _context.Goal.ToListAsync();
            var goalDTOs = _mapper.Map<List<GoalDTO>>(goalsList);
            return Ok(goalDTOs);
        }

        // GET: api/Goals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GoalDTO>> GetGoal(int id)
        {
            var goal = await _context.Goal.FindAsync(id);

            if (goal == null)
            {
                return NotFound();
            }

            var goalDTO = _mapper.Map<GoalDTO>(goal);

            return goalDTO;
        }

        // PUT: api/Goals/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGoal(int id, GoalDTO goalDTO)
        {
            Goal goalModel = _mapper.Map<Goal>(goalDTO);
            if (id != goalModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(goalModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GoalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Goals
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<GoalDTO>> PostGoal(GoalDTO goalDTO)
        {
            Goal goalModel = _mapper.Map<Goal>(goalDTO);
            _context.Goal.Add(goalModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGoal", new { id = goalModel.Id }, goalDTO);
        }

        // DELETE: api/Goals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<GoalDTO>> DeleteGoal(int id)
        {
            var goal = await _context.Goal.FindAsync(id);
            if (goal == null)
            {
                return NotFound();
            }

            _context.Goal.Remove(goal);
            await _context.SaveChangesAsync();

            GoalDTO goalDTO = _mapper.Map<GoalDTO>(goal);

            return goalDTO;
        }

        private bool GoalExists(int id)
        {
            return _context.Goal.Any(e => e.Id == id);
        }
    }
}
