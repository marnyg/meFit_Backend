﻿using meFIT.BackendAPI.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class ProgramRepository : EfCoreRepository<Models.Program, AppDbContext>
    {
        public ProgramRepository(AppDbContext context) : base(context)
        {
        }
    }
}
