﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class ExerciseRepository : EfCoreRepository<Exercise, AppDbContext>
    {
        public ExerciseRepository(AppDbContext context) : base(context)
        {
        }
    }
}
