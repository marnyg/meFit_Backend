﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class UserRepository : EfCoreRepository<User, AppDbContext>
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }
    }
}
