﻿using meFIT.BackendAPI.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class WorkoutRepository : EfCoreRepository<WorkoutRepository, AppDbContext>
    {
        public WorkoutRepository(AppDbContext context) : base(context)
        {
        }
    }
}
