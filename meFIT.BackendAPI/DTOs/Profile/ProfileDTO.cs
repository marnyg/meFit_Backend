﻿using meFIT.BackendAPI.DTOs.Goal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public class ProfileDTO
    {
        public int Id { get; set; }
        public float Weight { get; set; }
        public float Height { get; set; }
        public string MedicalConditions { get; set; }
        public string Disabilities { get; set; }
    }
}
