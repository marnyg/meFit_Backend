﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public class GoalWorkoutDTO
    {
        public int Id { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime ScheduleDate { get; set; }

    }
}
