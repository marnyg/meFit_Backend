﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class WorkoutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
