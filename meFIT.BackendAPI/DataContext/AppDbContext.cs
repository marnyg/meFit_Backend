﻿using meFIT.BackendAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DataContext
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
        { }

        #region Entities
        public DbSet<Address> Address { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Exercise> Exercise { get; set; }
        public DbSet<ExerciseCategory> ExerciseCategory { get; set; }
        public DbSet<Goal> Goal { get; set; }
        public DbSet<GoalWorkout> GoalWorkout { get; set; }
        public DbSet<Profile> Profile { get; set; }
        public DbSet<Models.Program> Program { get; set; }
        public DbSet<ProgramCategory> ProgramCategory { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkout { get; set; }
        public DbSet<Set> Set { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Workout> Workout { get; set; }
        public DbSet<WorkoutCategory> WorkoutCategory { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region FluentAPI examples
            // one-to-one
            //builder.Entity<Student>()
            //    .HasOne<StudentAddress>(s => s.Address)
            //    .WithOne(ad => ad.Student)
            //    .HasForeignKey<StudentAddress>(ad => ad.AddressId);

            // one-to-many
            //builder.Entity<Student>()
            //    .HasOne<Grade>(s => s.Grade)
            //    .WithMany(g => g.Students)
            //    .HasForeignKey(s => s.GradeId);

            // many-to-many
            //builder.Entity<StudentCourse>().HasKey(sc => new { sc.StudentId, sc.CourseId });

            //builder.Entity<StudentCourse>()
            //    .HasOne<Student>(sc => sc.Student)
            //    .WithMany(s => s.StudentCourses)
            //    .HasForeignKey(sc => sc.StudentId);


            //builder.Entity<StudentCourse>()
            //    .HasOne<Course>(sc => sc.Course)
            //    .WithMany(s => s.StudentCourses)
            //    .HasForeignKey(sc => sc.CourseId);
            #endregion

            #region GoalWorkout
            builder.Entity<GoalWorkout>().HasKey(props => 
                new { props.GoalId, props.WorkoutId }
            );

            builder.Entity<GoalWorkout>()
                .HasOne<Goal>(navs => navs.Goal)
                .WithMany(navs => navs.GoalWorkouts)
                .HasForeignKey(props => props.GoalId);
            #endregion

            #region ProgramWorkout
            builder.Entity<ProgramWorkout>().HasKey(props =>
                new { props.ProgramId, props.WorkoutId }
            );

            builder.Entity<ProgramWorkout>()
                .HasOne<Models.Program>(navs => navs.Program)
                .WithMany(navs => navs.ProgramWorkouts)
                .HasForeignKey(props => props.ProgramId);
            #endregion

            #region ProgramCategory
            builder.Entity<ProgramCategory>().HasKey(props =>
                new { props.ProgramId, props.CategoryId }
            );

            builder.Entity<ProgramCategory>()
                .HasOne<Models.Program>(navs => navs.Program)
                .WithMany(navs => navs.ProgramCategories)
                .HasForeignKey(props => props.ProgramId);
            #endregion

            #region ExerciseCategory
            builder.Entity<ExerciseCategory>().HasKey(props =>
                new { props.ExerciseId, props.CategoryId }
            );

            builder.Entity<ExerciseCategory>()
                .HasOne<Exercise>(navs => navs.Exercise)
                .WithMany(navs => navs.TargetMuscleGroups)
                .HasForeignKey(props => props.ExerciseId);
            #endregion

            #region WorkoutCategory
            builder.Entity<WorkoutCategory>().HasKey(props =>
                new { props.WorkoutId, props.CategoryId }
            );

            builder.Entity<WorkoutCategory>()
                .HasOne<Workout>(navs => navs.Workout)
                .WithMany(navs => navs.WorkoutCategories)
                .HasForeignKey(props => props.WorkoutId);
            #endregion
        }
    }
}
